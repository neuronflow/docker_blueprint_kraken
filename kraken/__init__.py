import os
# load env
from dotenv import load_dotenv
# don't touch this - can't touch this :7

load_dotenv(verbose=True)


def isKraken():
    KRAKEN = os.environ.get('KRAKEN', False)

    APPLICATION_NAME = os.getenv("APPLICATION_NAME")
    APPLICATION_VERSION = os.getenv("APPLICATION_VERSION")

    if KRAKEN:
        print(APPLICATION_NAME, 'running on entacle!')
        INPUT_FOLDER = os.getenv("INPUT_FOLDER_DOCKER")
        OUTPUT_FOLDER = os.getenv("OUTPUT_FOLDER_DOCKER")
    else:
        INPUT_FOLDER = os.getenv("INPUT_FOLDER")
        OUTPUT_FOLDER = os.getenv("OUTPUT_FOLDER")

    return INPUT_FOLDER, OUTPUT_FOLDER, APPLICATION_NAME, APPLICATION_VERSION


INPUT_FOLDER, OUTPUT_FOLDER, APPLICATION_NAME, APPLICATION_VERSION = isKraken()

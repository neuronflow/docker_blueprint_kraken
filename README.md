# README

read this carefully, for feedback / questions contact florian

This is the blueprint structure for kraken applications. Use the respective Dockerfiles for CUDA/CPU applications.

## copy paste your code

look through the repo. fill in the code at the places indicated by the comments.
pay special attention to `.env` and `.Dockerfile` and fill in your author information and application information there

## system specs

CUDA 10.1
Ubuntu 18.04
Python 3.6.9

## building docker image and saving it

follow the instructions above, please

### first build docker image

CPU:

```
docker build -t your_application_name . -f Dockerfile_CPU

```

GPU:

```
docker build -t your_application_name . -f Dockerfile_CUDA

```

### run it for testing
CPU:
```
docker run -it --rm --name your_container_name -v "/your/input/folder/":"/app/data/input" -v "/your/output/folder":"/app/data/output/" your_application_name
```

GPU:
```
docker run -it --rm --gpus device=0 --name your_container_name -v "/your/input/folder/":"/app/data/input" -v "/your/output/folder":"/app/data/output/" your_application_name
```

### save docker image

```
docker save your_application_name | gzip > ./docker_builds/your_application_name.tar.gz
```

## deploy

contact florian on slack to discuss deployment.
